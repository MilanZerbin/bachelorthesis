\documentclass{partials}

\begin{document}
\section{polytope Theory}
This section of the thesis contains an introduction to polytope theory towards an understanding of the main object of this thesis, a cyclic polytope as a counterexample to the local darboux theorem in PL-symplectic geometry. It is mainly based on chapter 0 of the well written book \emph{lectures on polytopes} by Günther M. Ziegler [REF], which takes the interested reader on a joyous tour threw the wonders of modern polytope theory.

\subsection{polytopes}
Recall that we call a subset $X \mathbb{R}^d$ of euclidian space \emph{convex}, if for any two points $x_1, x_2 \in X$ the connecting line segment $x_1 + \lambda(x_2 - x_1)$ for $\lambda\in [0,1]$ completely lies in $X$. We call the points on this connecting line segment \emph{convex combinations} of $x_1$ and $x_2$.
For our purposes, a $d$-dimensional polytope will then be defined as the convex hull of $n>d$ Points in $\mathbb{R}^d$. The convex hull of a finite set of Points $x_i\in \mathbb{R}^d$ is defined as the set
\begin{equation*}
\begin{split}
\operatorname{conv}\left\{ x_1, \cdots ,x_n \right\} \coloneqq \left\{ \sum\limits_{ i=1 }^{ n }{ \lambda_i x_i } \:\middle\vert \:\sum\limits_{ i=1 }^{ n }{ \lambda_i } = 1 \right\}
\end{split}
\end{equation*}
The following Lemma shows that the somewhat technical definition matches the geometric idea one might have of a \emph{convex hull}:
\begin{thesislemma}
The convex hull of the Points $X = \{x_1, \cdots, x_n\}$ is precicely the smallest convex set containing the $x_i$ for $i \in [n]$.
\end{thesislemma}
\begin{prf}
We call the smallest convex set containing our given Points $S(X)$ and now prove the inclusions $S(X) \subset \operatorname{conv}(X)$ (i) and $\operatorname{conv}(X) \subset S(X)$ (ii).
\begin{enumerate}[label=(\roman*)]
\item We can easily see that $\operatorname{conv}(X)$ is actually convex just by the definition. Thus $S\subset \operatorname{conv}(X) $ since we defined $S(X)$ to be the \emph{smallest} convex set containing $X$ and $\operatorname{conv}(X)$ contains $X$.
\item This is shown by an inductive argument over $k$: Setting $k=1$ we get the Points in $X$, which lie in $S(X)$ by definition. Assuming that the convex combinations of $k$ Points lie in $S(X)$, we use the convexity of $S(X)$ to write a convex combination of $k+1$ Points as one of two Points in $S(X)$ by
\begin{equation*}
\begin{split}
\lambda_1 x_1 + \cdots + \lambda_{k+1} x_{k+1} = (1-\lambda_{k+1}) \underbrace{\left( \frac{\lambda_1}{1-\lambda_{k+1}}x_1 + \cdots + \frac{\lambda_k}{1-\lambda_{k+1}} x_k\right)}\limits_{\in S(X)} + \lambda_{k+1} \underbrace{x_{k+1}}\limits_{\in S(X)}
\end{split}
\end{equation*}
Thus we see, using the convexity of $S(X)$, that indeed $\operatorname{conv}(X) \subset S(X)$ concluding the proof.
\end{enumerate} 
\end{prf}
Our Definition of polytope is now very geometric, but not very convenient to work with. A more easy to work with definition of a convex polytope is as a bounded intersection of affine Halfspaces \begin{equation*}
\begin{split}
\mathcal{H}_f \coloneqq \left\{ x \in \mathbb{R}^d \middle\vert f(x) \leq b \right\}
\end{split}
\end{equation*}
where the map $f\colon \mathbb{R}^d \to \mathbb{R}$ is an affine map. We call an inequality $f(x) \leq b$ characteristic for the resulting polytope, if there is no $b^\prime < b$ with $f(x)\leq b^\prime$ also being true for all points in the polytope. Equally one could define characteristic inequalities as the ones which actually are equalities for some points in $P$ (IMG). Then we can identify the caracteristic inequalities with the faces of the polytope, as shown in the Image. Note that for a given face, multiple characteristic inequalities might exist.\\
The two given definitions of polytopes are indeed equivalent, but the proof of this statement involves a more careful discussion of the matter than useful for this thesis. Thus we refer to chapter 1 of the book [REF], where the neccesary techniques are laid out and a proof is given.\\
A $d$-polytope is called \emph{simplicial} if any collection of $d$ vertices is affinely independet. This notion coincides with the geometric one that every top dimensional face is a simplex (REF). Since simplicial polytopes are much easier to work with and are dense with regard to the Hausdorff metric in the set of all polytopes (REF:gb), we include simplicial from this point on into the definition of polytope. Many Results from the study of simplicial polytopes also carry over to all polytopes, such as the important (Ref).

\subsection{Affine maps}
A natural question to ask is, what the right kind of maps between polytopes are. Or more formally, what Category we can build out of polytopes that is sort of well behaved. The answer, very unsurprisingly, lies in the affine linear maps. Those are the maps of the form $x \mapsto Ax + b$, where $A$ is a linear map and $b \in \mathbb{R}^d$. Then we indeed have a well behaved category $\mathbf{P}$ of convex polytopes and affine maps between them.\\
It is easily verified that affine maps preserve convexity, and even more so send affine halfspaces to affine halfspaces. We call an affine map $Ax + b$ an isomorphism, if the corresponding linear map $A$ is an isomorphism of vector spaces, and from now on only look at polytopes up to isomorphism. The equivalence class with regard to affine isomorphisms of a polytope $P$ is called \emph{combinatorial type} or \emph{combinatorial structure} of $P$, because an affine isomorphism of polytopes sends faces to faces.

\subsection{Cyclic polytopes}
Let us now move on to the class of polytopes relevant for this thesis: The so called \emph{cyclic polytopes}. We define the \emph{moment curve} $\gamma \colon \mathbb{R} \to \mathbb{R}^d$ in dimension $d$ by
\begin{equation*}
\gamma (t) = 
\begin{pmatrix}
t\\
t^2\\
\vdots\\
t^d\\
\end{pmatrix}
\end{equation*}
Then we define a cyclic polytope $C_n^d(t_1, \cdots, t_n)$ of order $n$ in dimension $d$ as the convex hull of the $n$ distinct points $\gamma(t_1), \cdots, \gamma(t_n)$ on the moment curve.
\begin{equation*}
C_n^d(t_1, \cdots, t_n) = \operatorname{conv}\left\{ \gamma(t_1), \cdots , \gamma(t_n) \right\}
\end{equation*}
The cases where $n\leq d$ are not polytopes in our definition above, so we shall exclude them from this point on. 
The main reason why cyclic polytopes are such interesting objects is captured in the following definition and theorem: A polytope is called \emph{$k$-neighbourly}, if all collections of $l \leq k$ vertices form a face of the polytope.
\begin{thesislemma}
Every cyclic polytope $C_n^d(t_1, \cdots, t_n)$ is $\left\lfloor \frac{d}{2} \right\lceil$-neighbourly
\end{thesislemma}
\begin{prf}
Given a subcollection consisting $l \leq \left\lfloor \frac{d}{2} \right\lceil$ timestamps $t_{i_1}, \cdots, t_{i_l}$, we now need to prove that the points $\gamma(t_{i_1}, \cdots, t_{i_l})$ form a face of $C_n^d(t_1, \cdots, t_n)$. We do this by showing there is an affine map that is zero on the face, and positive for the rest of the polytope, thereby giving a characteristic halfspace $\mathcal{H}$ for the face:
Consider the polynomial
\begin{equation*}
\begin{split}
p(t) = \prod\limits_{k=1}^{l} (t - t_{i_k})^2 = \beta_0 + t\beta_1 + \cdots + \beta_{2l} t^{2l}
\end{split}
\end{equation*}
We then define the $d$- vector $\beta$ as
\begin{equation*}
\begin{split}
\beta = (\beta_1, \beta_2, \cdots ,\beta_{2l}, 0, \cdots , 0)
\end{split}
\end{equation*}
The characteristic map we are looking for is given by $f(x) = \langle x , \beta \rangle + \beta_0$. Then we have $f(\gamma(t)) = p(t)$, and therefore it instantly follows that $f(\gamma(t_i)) > 0$ für $i\neq i_l$ and $f(\gamma(t_i)) = 0$ für $i = i_l$ for some $l$.  
\end{prf}
A direct corollary of this lemma is that all cyclic polytopes are simplicial (every face is a simplex), since all vertices are connected by edges which is equivalent to being $1$-neighbourly.
Since it is annoying to always keep track of the exact choice of $t_1, \cdots, t_n$ the following characterisation of the combinatorial type of a convex polytope is very helpful. It proves that the combinatorial type of a cyclic polytope only depends on $n$ and $d$, thus allowing us to speak of \emph{the} cyclic polytope $C_n^d$ and omitting the $t_1, \cdots, t_n$ in the notation from this point on.
\begin{thesislemma}[Gale eveness condition]
A subset of $d-1$ points $\{\gamma(t_{j})\:\vert\: j \in J \}$ on the moment curve is a face of $C_n^d(t_1, \cdots , t_n)$ precicely if any two timestamps $t_i$ with $i \not\in J$ have an even number of $t_j$ with $j\in J$ between them. 
\end{thesislemma}
\begin{prf}
Consider the affine map
\begin{equation*}
\begin{split}
F(v) = \det 
\begin{pmatrix}
1 & 1 & \cdots & 1 \\
v & \gamma(t_{j_1}) & \cdots & \gamma(t_{j_{d-1}})
\end{pmatrix}
\end{split}
\end{equation*}
\end{prf}
\end{document}
