\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}

\LoadClass[a4paper]{scrartcl}

% packages
\RequirePackage[utf8]{inputenc}
\RequirePackage{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{microtype}
\RequirePackage[pdfpagelabels=true]{hyperref}
\RequirePackage{tikz}
\RequirePackage{tikz-cd}
\usetikzlibrary{arrows, calc, cd}
\RequirePackage{amsmath, amssymb, amsthm, amsfonts, mathtools}
\RequirePackage{array}
\RequirePackage{graphicx}
\RequirePackage{enumitem}
\RequirePackage{interval}

% package configs
\intervalconfig{soft open fences}
\geometry{a4paper, top=27mm, left=27mm, right=18mm, bottom=35mm, headsep=10mm, footskip=12mm}

% theorems
\newtheoremstyle{thesistheoremstyle}% name of the style to be used
  {\topsep}% measure of space to leave above the theorem. E.g.: 3pt
  {1cm}% measure of space to leave below the theorem. E.g.: 3pt
  {\upshape}% name of font to use in the body of the theorem
  {0pt}% measure of space to indent
  {\bfseries}% name of head font
  {}% punctuation between head and body
  { }% space after theorem head; " " = normal interword space
  {\thmname{#1}\thmnumber{ #2}\textnormal{\thmnote{ (\textit{#3})}}}

\theoremstyle{thesistheoremstyle}
\newtheorem{thesisproposition}{Proposition}
\newtheorem{thesislemma}{Lemma}

\theoremstyle{plain}
\newtheorem*{theorem}{Theorem}
\newtheorem*{prf}{Proof}

% some shorthands
\let\implies\Rightarrow
\let\iff\Leftrightarrow
\newcommand{\N}{\ensuremath{\mathbb{N}}} % natural numbers
\newcommand{\Z}{\ensuremath{\mathbb{Z}}} % integers
\newcommand{\R}{\ensuremath{\mathbb{R}}} % reals
\newcommand{\Q}{\ensuremath{\mathbb{Q}}} % rational numbers
\newcommand{\K}{\ensuremath{\mathbb{K}}} % generic field
\newcommand{\C}{\ensuremath{\mathbb{C}}} % complex numbers
\newcommand{\eps}{\ensuremath{\varepsilon}}
\newcommand{\adjunction}[2]{\ensuremath{#1 \left[ #2 \right]}}
\newcommand{\im}{\mathrm{i}}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}


% declare absolute value, norm, ...
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\innerproduct}{\langle}{\rangle}%

\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\let\oldinnerproduct\innerproduct
\def\innerproduct{\@ifstar{\oldinnerproduct}{\oldinnerproduct*}}
\makeatother


% declare math operators
\DeclareMathOperator{\lspan}{span} % linear span


% Nicer looking quantifiers
\newcommand{\fa}[1]{\forall \, {#1} \,:\,}
\newcommand{\ex}[1]{\exists \, {#1} \,:\,}
\newcommand{\exu}[1]{\exists! \, {#1} \,:\,} % Eindeutige Existenz

\newcommand\restr[2]{{% we make the whole thing an ordinary symbol
  \left.\kern-\nulldelimiterspace % automatically resize the bar with \right
  #1 % the function
  \right|_{#2} % this is the delimiter
  }}

%quotients
 \newcommand\quotient[2]{
        \mathchoice
            {% \displaystyle
                \text{\raise1ex\hbox{$#1$}\Big/\lower1ex\hbox{$#2$}}%
            }
            {% \textstyle
                #1\,/\,#2
            }
            {% \scriptstyle
                #1\,/\,#2
            }
            {% \scriptscriptstyle  
                #1\,/\,#2
            }
    }

% equal signs with stuffs on top
\newcommand\eqdef{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily def}}}{=}}}
\newcommand\eqstar{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily $\circledast$}}}{=}}}

% roman numerals
\newcommand{\RN}[1]{\uppercase\expandafter{\romannumeral #1\relax}}
\newcommand{\rn}[1]{\lowercase\expandafter{\romannumeral #1\relax}}

% penalties
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000
