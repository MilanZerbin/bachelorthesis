\documentclass[11pt,a4paper]{beamer}


%needed Packages
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{wasysym}
\usepackage{stmaryrd}
\usepackage{csquotes}
\usepackage{tikz-cd}
\usepackage{datetime}
\usepackage{etoolbox}
%end needed packs

%theme
\usetheme{Montpellier}
\usecolortheme{lily}
%theme used

\hypersetup{
  pdftitle    = {Bachelorarbeit},
  pdfsubject  = {On a local Darboux Theorem in piecewise linear symplectic Geometry},
  pdfauthor   = {Milan Zerbin},
  pdfkeywords = {PL, Darboux},
  pdfcreator  = {pdflatex},
  pdfproducer = {LaTeX}
  colorlinks  = true,
  linkcolor   = red!80!black,
  anchorcolor = red!80!black,
  citecolor   = green!60!black,
  filecolor   = red!80!black,
  runcolor    = red!80!black,
  urlcolor    = red!60!black,
}

%my title and stuffs
\dmyydate
\title{On a local Darboux Theorem in piecewise linear symplectic Geometry}
\institute{Bachelor thesis}
\author{Milan Zerbin}
\date{28.07.21}
%end of title stuffs

%Preamble
\setbeamertemplate{section in toc}[sections numbered]

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Fortschritt}
    \tableofcontents[hideothersubsections, sectionstyle=show/hide, subsectionstyle=show/shaded/hide,  subsubsectionstyle=show/shaded/hide/hide]
  \end{frame}
}

%end Preamble

%Environments
\newtheorem{satz}{Satz:}[section]
\newtheorem{lem}{Lemma:}[section]
\newtheorem{cor}{Korollar:}[section]
%end Environments


%custom commands and stuffs



%% Nice to have
% some shorthands
\let\implies\Rightarrow
\let\iff\Leftrightarrow
\newcommand{\N}{\ensuremath{\mathbb{N}}} % natural numbers
\newcommand{\Z}{\ensuremath{\mathbb{Z}}} % integers
\newcommand{\R}{\ensuremath{\mathbb{R}}} % reals
\newcommand{\Q}{\ensuremath{\mathbb{Q}}} % rational numbers
\newcommand{\K}{\ensuremath{\mathbb{K}}} % generic field
\newcommand{\C}{\ensuremath{\mathbb{C}}} % complex numbers
\newcommand{\id}{\ensuremath{\operatorname{id}}} % identity
\newcommand{\eps}{\ensuremath{\varepsilon}}
\newcommand{\adjunction}[2]{\ensuremath{#1 \left[ #2 \right]}}
\newcommand{\im}{\mathrm{i}}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}

% quotes and quotations
\renewcommand{\enquote}[1]{\textquote{#1}}
\newcommand{\ironic}[1]{\textsl{#1}}
\renewcommand{\emph}[1]{\textit{#1}}

% declare absolute value, norm, ...
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\simp}{\langle}{\rangle}

\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\let\oldsimp\simp
\def\simp{\@ifstar{\oldsimp}{\oldsimp*}}
\makeatother


% declare math operators
\DeclareMathOperator{\lspan}{span} % linear span
\newcommand{\st}[2]{\operatorname{st}_{#1}\left( #2 \right)}
\newcommand{\lk}[2]{\operatorname{lk}_{#1}\left( #2 \right)}

% piecewise linear and other specifics
\newcommand{\pl}{PL }
\newcommand{\pc}{PC }
\newcommand{\cpst}{\operatorname{st}}
\newcommand{\cplk}{\operatorname{lk}}
\newcommand{\triang}{\triangleleft}
\newcommand{\sdivs}{\prec}
\newcommand{\pleq}{\cong}

% Nicer looking other stuffs
\newcommand{\ie}{i.\@ e.\,}

% Nicer looking quantifiers
\newcommand{\fa}[1]{\forall \, {#1} \,:\,}
\newcommand{\ex}[1]{\exists \, {#1} \,:\,}
\newcommand{\exu}[1]{\exists! \, {#1} \,:\,} % Eindeutige Existenz

\newcommand\restr[2]{{% we make the whole thing an ordinary symbol
  \left.\kern-\nulldelimiterspace % automatically resize the bar with \right
  #1 % the function
  \right|_{#2} % this is the delimiter
  }}

% quotients
 \newcommand\quotient[2]{
        \mathchoice
            {% \displaystyle
                \text{\raise1ex\hbox{$#1$}\Big/\lower1ex\hbox{$#2$}}%
            }
            {% \textstyle
                #1\,/\,#2
            }
            {% \scriptstyle
                #1\,/\,#2
            }
            {% \scriptscriptstyle  
                #1\,/\,#2
            }
    }

% equal signs with stuffs on top
\newcommand\eqdef{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily def}}}{=}}}
\newcommand\eqstar{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily $\circledast$}}}{=}}}

% roman numerals
\newcommand{\RN}[1]{\uppercase\expandafter{\romannumeral #1\relax}}
\newcommand{\rn}[1]{\lowercase\expandafter{\romannumeral #1\relax}}


%end custom commands and stuffs


\begin{document}

\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Structure of the talk}
\tableofcontents
\end{frame}

\section{What is the ordinary Darboux Theorem?}
\begin{frame}{Darboux Theorem}
On a Symplectic Manifold $(M,\omega)$ we may at every point $p\in M$ choose a chart $\phi\colon M \supset U \to \R^{2n}$ such that 
\begin{equation*}
	\restr{\omega}{U} = \phi^\ast \left( dx_1 \wedge dy_1 + \cdots + dx_n \wedge dy_n \right)
\end{equation*}
where $U$ is a sufficiently small Neighbourhood of $p$.
\end{frame}

\section{The piecewise linear World}
\begin{frame}{Roadmap}
\centering
Abstract simplicial complexes
\begin{equation*}
\qquad\qquad \downarrow \textnormal{\tiny Geometric realization}
\end{equation*}
Polyhedra
\begin{equation*}
\qquad\downarrow \textnormal{\tiny Local Model}
\end{equation*}
PL Manifolds
\begin{equation*}
\quad\downarrow \textnormal{\tiny PC Form}
\end{equation*}
PL symplectic Manifolds
\end{frame}

\begin{frame}{Simplicial complexes}
\begin{definition}[Simplicial complex]
A simplicial complex is a finite set $V$ together with a subset $S$ of the powerset $\mathcal{P}(V)$ of the so called vertex set $V$, such that the following hold:
\begin{itemize}
\item[(\rn{1})] All singletons $\{v\}$ for $v\in V$ are elements in $S$.
\item[(\rn{2})] For all $\sigma \in S$ and for all $\tau \subset \sigma$ we have $\tau \in S$. 
\end{itemize}
\end{definition}
\end{frame}

\begin{frame}{Simplicial maps}
\begin{definition}[Simplicial map]
A map $f\colon (V, S) \to (W, T)$ of simplicial complexes is just a map of the underlying vertex sets $V \to W$. It is called simplicial if and only if $f(\sigma) \in T$ for each $\sigma \in S$. A (simplicial) map like $f$ is called (simplicial) isomorphism if it has an inverse (simplicial) map $g$, such that $f\circ g = \id_W$ and $g\circ f = \id_V$.
\end{definition}
\end{frame}

\begin{frame}{Geometric realization}
\begin{definition}[Geometric realization]
Given a simplicial complex $K= (V,S)$ we call the following space the geometric realization of $K$:
\begin{equation*}
\abs{K} \coloneqq \left\{ V \overset{\rho}{\to} [0,1] \:\middle\vert\: \operatorname{supp}(\rho) \in S \textnormal{ and } \int_V \rho \mathrm{d}\mu = 1 \right\}
\end{equation*}
where the integral is taken with respect to the counting measure, making it just a shorthand for writing out a sum.
As a topology on $\abs{K}$ we choose the one induced by the $L^2$ norm.
\end{definition}
\end{frame}

\begin{frame}[fragile]{Piecewise linear maps}
\begin{definition}[Piecewise linear map]
A continuous map $f\colon \abs{K} \to \abs{L}$ between two polyhedra is called \pl if there exist subdivisions $K^\prime \sdivs_\kappa K$ and $L^\prime \sdivs_\lambda L$ such that $\lambda^{-1}\circ f \circ \kappa = \abs{g}$ for some simplicial map $g\colon K^\prime \to L^\prime$.
\end{definition}
\begin{center}
\begin{tikzcd}[ampersand replacement=\&]
\abs{K}
\arrow{r}{f}
 \& \abs{L} \\
\abs{K^\prime}
\arrow{u}{\kappa}
\arrow{r}[swap]{\abs{g}}
 \& \abs{L^\prime}
   \arrow{u}[swap]{\lambda}
\end{tikzcd}
\end{center}
\end{frame}

\begin{frame}{PL Manifolds}
Spaces that \textit{locally} look like Polyhedra
\begin{definition}[\pl structure]
A \pl structure $\mathcal{L}$ on a topological space $X$ is a collection of topological embeddings (called \emph{coordinate maps}) of the form $q\colon Q \to X$ where $Q$ is a polyhedron, such that 
\begin{itemize}
\item[(\rn{1})] The images of the coordinate maps in $\mathcal{L}$ cover $X$.
\item[(\rn{2})] given any two coordinate maps $q_i\colon Q_i \to X$ with nonempty intersection $V = q_1(Q_1)\cap q_2(Q_2)$ we have a coordinate map $p\colon P\to X$ satisfying $p(P) = V$ and the maps $q_i^{-1} \circ p$ are \pl maps of polyhedra. We then say, that the two maps $q_i$ are \emph{compatible}.
\item[(\rn{3})] The collection $\mathcal{L}$ is \emph{maximal}, \ie every coordinate map $p\colon P \to X$  compatible with all maps in $\mathcal{L}$ is in $\mathcal{L}$.
\end{itemize}
\end{definition}
\end{frame}

\begin{frame}{PL structure}
\centering
\includegraphics[width=.8\textwidth]{../img/plspaceglue.png}
\end{frame}

\begin{frame}{PL Darboux}
Given a piecewise constant nondegenerate $2$-form $\omega$ on a \pl manifold $M$, can we find a chart $\phi\colon M\supset U \to \R^n$ with
\begin{equation*}
\phi^\ast \omega_0 = \restr{\omega}{U} \:?
\end{equation*}
Do combinatorial constraints arise?
\end{frame}

\section{Cyclic polytopes}
\begin{frame}{Moment curve}
We define the \emph{moment curve} $\gamma \colon \mathbb{R} \to \mathbb{R}^d$ in dimension~$d$ by
\begin{equation*}
\gamma (t) = 
\begin{pmatrix}
t\\
t^2\\
\vdots\\
t^d\\
\end{pmatrix}
\end{equation*}
Then we define a cyclic polytope $C_n^d(t_1, \cdots, t_n)$ of order $n$ in dimension~$d$ as the convex hull of the $n$ distinct points $\gamma(t_1), \cdots, \gamma(t_n)$ on the moment curve.
\end{frame}

\begin{frame}{Cyclic polytope}
\centering
\includegraphics[width=.6\textwidth]{../tex/tikz/c64.pdf}
\end{frame}

\end{document}
